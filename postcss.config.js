module.exports = (ctx) => ({
    plugins: {
        "postcss-import": {},
        "postcss-import-url": {},
        tailwindcss: {},
        autoprefixer: {},
        cssnano: ctx.env === "production" ? {} : false,
    },
});
