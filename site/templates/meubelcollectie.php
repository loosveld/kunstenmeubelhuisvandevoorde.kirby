<?php snippet('header', array('robots' => 'index, follow')) ?>


<main class="pt-12 container" id="meubelcollectie" >

    <?php snippet('toggler'); ?>

    <div class="lg:flex">
        
        <div class="mb-16 lg:w-5/12 " id="info">
            <a href="<?php echo $site->url((string) $site->language()) ?>" class="block mb-8">
                <img src="/assets/images/Vandevoorde-Meubelhuis-wit-zwart.svg" class="logo "
                    alt="<?php echo $site->title(); ?>">
            </a>

            <a href="/#meubelcollecties" class="underline-from-left leading-tight text-xl lg:text-base">Terug naar homepage</a>

            <h1 class="mt-0 mb-8 leading-tight text-5xl"><?= $page->title()->html() ?></h1>
    
            <?php if (!$page->text()->empty()) : ?>
            <div id="info"><?= $page->text()->kirbytext() ?></div>
            <?php endif; ?>
    
            <h3 class="mt-8">Andere meubelstijlen</h3>

            <div class="flex items-center mt-4">
                <div class="md:flex flex-wrap -mx-2">
                    <?php foreach ($page->siblings() as $sibling) : ?>
                        <a href="/<?= $sibling; ?>" class="collectie with-arrow-right block  mt-4 text-white mx-2 object-cover no-underline" id="">
                            <figure>
                                <figcaption class="cursor-pointer <?php echo $page->is($sibling) ? 'bg-white text-red': 'bg-red text-white' ?>  lg:flex items-center justify-between btn font-title p-4  no-underline uppercase font-bold text-sm">
                                <?= $sibling->title(); ?>
                                </figcaption>
                            </figure>
                        </a>
                    <?php endforeach ?>
                </div>

            </div>
        </div>
    
        <div class="lg:w-7/12 lg:ml-12 mb-8 masonry"  id="gallery">
            <?php foreach ($page->images() as $image) : ?>
            <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="mb-4">
                <a href="<?php echo $image->resize(2000)->url(); ?>" title="<?= $image->caption() ?> "><img
                        src="<?php echo $image->resize(500)->url(); ?>" alt="<?= $image->caption() ?>"></a>
            </figure>
            <?php endforeach ?>
        </div>

    </div>
        
    
</main>

<?php snippet('footer'); ?>



<?php snippet('scripts'); ?>