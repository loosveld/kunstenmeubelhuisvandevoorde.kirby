<?php snippet('header', array('robots' => 'index, follow')) ?>

<main class="mx-auto">

	<div class="clearfix flex justify-center">
		<div class="pt4 pb4 mt4 max-width-3">
			<h1 class="pt4"><?= $page->title()->html() ?></h1>
			<?= $page->text()->kirbytext() ?>
		</div>
	</div>
	<div class="redbg text-white ">
		<h2 class="pt4">Ontdek onze collecties in uitverkoop</h2>
		<div class="max-width-4 mx-auto pb4 flex flex-wrap items-center justify-around">

		<?php foreach($site->find('meubelcollecties/tijdloze-stijl', 'meubelcollecties/slaapcomfort', 'meubelcollecties/landelijke-stijl', 'meubelcollecties/engelse-meubelcollectie') as $meubelcollectie): ?>
			<p class="bg-white text-black rounded px1 py1 mb1 mr1 flex-auto"><a href="<?= $meubelcollectie->url() ?>"><?= html($meubelcollectie->title()) ?></a></p>
		<?php endforeach ?>
		</div>
	</div>



	<div class="clearfix flex justify-center">
		<div class="py4 max-width-4">
			<?= $page->text2()->kirbytext() ?>
			<img src="<?php echo $page->image()->url() ?>" class="fit">	
		</div>
	</div>

	

<?php snippet('contact') ?>

</main>

<?php snippet('footer') ?>