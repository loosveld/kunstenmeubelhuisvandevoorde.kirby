<?php snippet('header', array('robots' => 'index, follow')) ?>


<main class="pt-12 container" id="meubelcollectie" data-barba="container" data-barba-namespace="kunstenaar">

    <?php snippet('toggler'); ?>

    <div class="lg:flex">

        <div class="mb-16 lg:w-5/12 " id="info">

            <a href="<?php echo $site->url((string) $site->language()) ?>" class="block mb-8">
                <img src="/assets/images/Vandevoorde-Meubelhuis-wit-zwart.svg" class="logo "
                    alt="<?php echo $site->title(); ?>">
            </a>

            <a href="/#meubelcollecties" class="underline-from-left leading-tight text-xl lg:text-base">Terug naar homepage</a>

            <h1 class="mt-0 mb-8 leading-tight text-5xl"><?= $tag ?></h1>

            <?php if (!$page->text()->empty()) : ?>
                <div id="info"><?= $page->text()->kirbytext() ?></div>
            <?php endif; ?>

            <h3 class="mt-8">Andere categorieën</h3>

            <div class="flex items-center mt-4">
                <?php snippet('meubelcategories', $categories); ?>
            </div>

        </div>
    
        <div class="lg:w-7/12 lg:ml-12 mb-8 masonry"  id="gallery">
            <?php foreach ($files as $file) : ?>
            <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="mb-4">
                <a href="<?php echo $file->resize(2000)->url(); ?>" title="<?= $file->caption() ?> "><img
                        src="<?php echo $file->resize(500)->url(); ?>" alt="<?= $file->caption() ?>"></a>
            </figure>
            <?php endforeach ?>
        </div>

    </div>

</main>



<?php snippet('scripts'); ?>