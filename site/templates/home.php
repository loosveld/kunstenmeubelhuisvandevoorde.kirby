<?php snippet('header', array('robots' => 'index, follow')); ?>

<main id="home" data-barba="container" data-barba-namespace="home">
    <div class="container">
        <?php snippet('toggler'); ?>
        <div class="lg:grid grid-cols-12 gap-16">
            <?php snippet('aside'); ?>
            <div class="sections lg:col-span-9">
                <section id="hero" class="pt-16 lg:pt-40 relative lg:mb-48">
                    <a href="<?php echo $site->url((string) $site->language()) ?>" <img
                        src="/assets/images/Vandevoorde-Meubelhuis-wit-zwart.svg" class="logo"
                        alt="<?php echo $site->title(); ?>">
                    </a>
                    <img src="<?php echo $site->images()->first()->url(); ?>"
                        class="hidden lg:block absolute top-0 md:w-7/12 xl:w-6/12 right-0" style="z-index:-1" />
                    <h1 class="uppercase text-black text-4xl lg:text-6xl">
                        <?php foreach($site->Title()->split(' ') as $word): echo '<div>'.$word.'</div>'; endforeach; ?>
                    </h1>
                    <div class="lg:hidden"><?php snippet('afspraak', ['hideModalContent' => true]) ?></div>
                </section>

                <?php snippet('introductie', ['page' => $site->find('introductie')]); ?>

                <?php snippet('meubelcollecties', ['page' => $site->find('meubelcollecties')]); ?>

                <?php if(count($categories)): ?>  
                    <section id="<?php echo $page->id(); ?>" class="mb-24 text-black">
                        <h3>ONZE CATEGORIEËN</h3>
                        <?php snippet('meubelcategories', $categories); ?>
                    </section>
                <?php endif; ?>

                <?php snippet('evenementen', ['page' => $site->find('evenementen')]); ?>
            </div>
        </div>
    </div>
    <?php snippet('footer'); ?>
</main>

<?php snippet('scripts'); ?>