<?php snippet('header', array('robots' => 'index, follow')); ?>

<main id="default" data-barba="container" data-barba-namespace="default">
  <div class="container">
    <h1><?= $page->title()->html() ?></h1>
    <?= $page->text()->kirbytext() ?>
  </div>
  <?php snippet('footer'); ?>
</main>

<?php snippet('scripts'); ?>