<?php snippet('header', array('robots' => 'index, follow')); ?>

<main id="home" data-barba="container" data-barba-namespace="home">
    <div class="container">
        <?php snippet('toggler'); ?>
        <div class="lg:grid grid-cols-12 gap-16">
            <?php snippet('aside'); ?>
            <div class="sections lg:col-span-9">
                <section id="hero" class="pt-16 lg:pt-40 relative lg:mb-48">
                    <a href="<?php echo $site->url((string) $site->language()) ?>" <img
                        src="/assets/images/Vandevoorde-Meubelhuis-wit-zwart.svg" class="logo"
                        alt="<?php echo $site->title(); ?>">
                    </a>

                    <h1 class="uppercase text-black text-4xl lg:text-6xl mt-6">
                        <?php foreach($page->Title()->split(' ') as $word): echo '<div>'.$word.'</div>'; endforeach; ?>
                    </h1>
                    <?php if (!$page->text()->empty()) : ?>
                    <div id="info"><?= $page->text()->kirbytext() ?></div>
                    <?php endif; ?>
                </section>



            </div>
        </div>
    </div>
    <?php snippet('footer'); ?>
</main>

<?php snippet('scripts'); ?>