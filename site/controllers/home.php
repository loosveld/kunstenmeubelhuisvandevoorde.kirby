<?php


return function($site, $pages, $page) {

    $meubelcollecties = $site->children()->find('meubelcollecties')->children();
    $categories = [];

    foreach ($meubelcollecties->files() as $file):
        if($file->typetag()->value() != ''):
            $cats = explode(",", $file->typetag()->value());
            foreach ($cats as $cat) :
                $categories[$cat] = [];
            endforeach;
        endif;
    endforeach;


    foreach ($categories as $key => $category):
        if($key):
            foreach ($meubelcollecties->files() as $file):
                if($file->typetag()->value == $key) :
                    array_push($categories[$key],$file);
                endif;
            endforeach;
        endif;
    endforeach;


    return array(
        'categories'   => $categories
    );
};