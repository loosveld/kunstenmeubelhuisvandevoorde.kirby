<?php


return function($site, $pages, $page) {

    $tag = explode("/",$_SERVER['REQUEST_URI'])[3];

    $meubelcollecties = $site->children()->find('meubelcollecties')->children();
    $categories = [];

    foreach ($meubelcollecties->files() as $file):
        if($file->typetag()->value() != ''):
            $cats = explode(",", $file->typetag()->value());
            foreach ($cats as $cat) :
                $categories[$cat] = [];
            endforeach;
        endif;
    endforeach;



    //$categories = array_unique($categories);



    $files = [];

    if($tag):
        foreach ($meubelcollecties->files() as $file):
            $cats = explode(",", $file->typetag()->value());
            foreach ($cats as $cat) :
                if($cat == $tag) :
                    array_push($files,$file);
                endif;
            endforeach;
        endforeach;
    endif;

    return array(
        'files'   => $files,
        'categories' => $categories,
        'tag' => $tag
    );
};