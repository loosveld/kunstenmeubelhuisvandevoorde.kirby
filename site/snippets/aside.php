<aside class="hidden lg:block col-span-3 pt-12">
    <div class="sticky" style="top:3rem">
        <a href="<?php echo $site->url((string) $site->language()) ?>" class="block mb-10">
            <img src="/assets/images/Vandevoorde-Meubelhuis-wit-zwart.svg" class="logo "
                alt="<?php echo $site->title(); ?>">
        </a>
        <?php snippet('menu') ?>
        <br>
        <?php snippet('afspraak', ['hideModalContent' => false]) ?>
    </div>
</aside>