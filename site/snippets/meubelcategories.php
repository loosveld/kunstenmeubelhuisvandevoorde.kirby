<div class="<?php echo $tag  ? 'md:grid grid-cols-12 gap-8': 'md:grid grid-cols-12 gap-8' ?>  mt-8"  >
  <?php foreach ($categories as $category => $files) : ?>
    <a href="<?= '/nl/categorie/'.$category; ?>" class="collectie with-arrow-right block bg-red text-white col-span-12  <?php echo $tag ? 'md:col-span-6 xl:col-span-4': 'md:col-span-6 xl:col-span-3' ?>  object-cover no-underline" id="">
      <figure>
        <figcaption class="cursor-pointer <?php echo $tag == $category ? 'bg-white text-red': 'bg-red text-white' ?>  lg:flex items-center justify-between btn font-title p-4  no-underline uppercase font-bold text-sm">
          <?= $category; ?>
          <svg height="30" width="30" viewBox="0 0 50 50" class="ml-4 inline">
            <polyline fill="none" stroke="white" stroke-width="2px" points="20,15 30,25 20,35" />
            <polyline fill="none" stroke="white" stroke-width="2px" points="0,25 30,25" />
          </svg>
        </figcaption>
      </figure>
    </a>
  <?php endforeach; ?>
</div>
