<!doctype html>
<html lang="<?= site()->language() ? site()->language()->code() : 'nl-BE' ?>">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <title><?= $page->title()->html() ?> | <?= $site->name()->html() ?></title>
    <meta name="robots" content="<?= $robots ?>" />
    <meta property="og:title" content="<?= $site->name()->html() ?> | <?= $page->title()->html() ?>" />
    <meta property="og:locale" content="nl_BE" />
    <meta property="og:url" content="<?= $page->url() ?>" />
    <meta property="og:type" content="website" />

    <?php if ($page->image()) : ?>
    <meta property="og:image" content="<?= $page->image()->url() ?>" />
    <?php else : ?>
    <meta property="og:image" content="/assets/images/Vandevoorde%20Meubelhuis.svg" />
    <?php endif ?>
    <?php if ($page->text() != '') : ?>
    <meta name="description" content="<?= $page->text() ?>" />
    <?php else : ?>
    <meta name="description" content="<?= $site->description()->kirbytext() ?>" />
    <?php endif ?>

    <?= js(["assets/js/loader.js"])
  ?>

    <?= css([
    'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css',
    'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/solid.min.css',
    'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/brands.min.css',
    'assets/css/style.min.css?v=1.4'
  ]) ?>

</head>

<body data-barba="wrapper">

    <!--[if IE]>
      <div class="warning"><p>For the best experience please open using Chrome, Firefox or Safari</p></div>
  <![endif]-->

    <div id="loader">
        <img src="/assets/images/tail-spin.svg" alt="Loader">
    </div>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K5GH5KM" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <figure id="background" class="background has-overlay">
        <img src="/assets/images/swirl.png" alt="" />
    </figure>

    <div id="transition" class="bg-red fixed w-full bottom-0 display-none z-20"></div>

    <?php if($site->user()):?>
    <?php snippet('languages'); ?>
    <?php endif; ?>

    <?php foreach ($site->find('evenementen')->children()->visible() as $evenement) : ?>
    <?php if (!$evenement->highlight()->empty()) : ?>
    <div id="notification" class="bg-red fixed bottom-0 z-10 w-screen text-white">
        <div class="container flex bg-red p-4 pr-10 font-bold">
            <a href="#<?php echo $evenement->id() ?>" class="no-underline"><?php echo $evenement->highlight(); ?></a>
        </div>
    </div>
    <?php endif; ?>
    <?php endforeach; ?>