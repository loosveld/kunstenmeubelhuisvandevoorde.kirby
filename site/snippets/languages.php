<?php if($site->languages && $site->languages->count() > 1): ?>

<div class="language-selector">
    <ul class="">
        <li><a href="#" aria-haspopup="true"><?php echo strtoupper($site->language()) ?></a>
            <ul class="dropdown" aria-label="submenu">
                <?php foreach($site->languages as $lang): ?>
                <?php if($lang != $site->language()):?>
                <li>
                    <a href="/<?php echo $lang->code() ?>"><?php echo strtoupper($lang->code()) ?></a>
                </li>
                <?php endif ?>
                <?php endforeach ?>
            </ul>
        </li>
    </ul>

</div>

<?php endif ?>