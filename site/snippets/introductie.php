  <section id="<?php echo $page->id(); ?>" class="mb-24">
      <h3><?php echo $page->title()->html() ?></h3>
      <div class="lg:grid grid-cols-12 bg-red mt-8">
          <div class="lg:col-span-7 xl:col-span-8 p-8 text-white">
              <?php echo $page->text()->kirbytext() ?>
          </div>
          <?php if (!$page->images()->empty()) : ?>
          <figure class="col-span-12 lg:col-span-5 xl:col-span-4">
              <img class="h-full w-full object-cover" src="<?= $page->images()->first()->url(); ?>">
          </figure>
          <?php endif; ?>
  </section> <!-- /introductie -->