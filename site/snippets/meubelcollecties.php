<?php if ($page->hasVisibleChildren()) : ?>
  <section id="<?php echo $page->id(); ?>" class="mb-24 text-black">
    <h3><?php echo $page->title()->html() ?></h3>
    <div class="md:grid grid-cols-12 gap-8">
      <?php foreach ($page->children()->visible() as $collectie) : ?>
        <a href="<?= $collectie->uri(); ?>" class="collectie with-arrow-right block bg-red mt-8 text-white col-span-12 md:col-span-6 xl:col-span-3 object-cover no-underline" id=" <?php echo $collectie->id() ?>">
          <?php if (!$collectie->coverimage()->empty()) : ?>
            <figure>
              <img class="w-full object-cover" src="<?= $collectie->images()->find($collectie->coverimage())->crop(400,400)->url() ?>">
              <figcaption class="cursor-pointer text-white lg:flex items-center justify-between btn font-title p-4 bg-red no-underline uppercase font-bold text-sm">
                <h2 class="font-title font-bold text-sm">
                  <?php echo $collectie->title()->html() ?>
                </h2>
                <svg height="30" width="30" viewBox="0 0 50 50" class="ml-4 inline">
                    <polyline fill="none" stroke="white" stroke-width="2px" points="20,15 30,25 20,35" />
                    <polyline fill="none" stroke="white" stroke-width="2px" points="0,25 30,25" />
                </svg>
              </figcaption>
            </figure>
          <?php endif; ?>
        </a>
      <?php endforeach; ?>
    </div>
  </section>
<?php endif; ?>
