<?php if ($page->hasVisibleChildren()) : ?>
<section id="<?php echo $page->id(); ?>" class="mb-24">
    <h3><?php echo $page->title()->html() ?></h3>
    <hr class="mt-4 mb-16">
    <div>
        <?php foreach ($page->children()->visible()->sortBy('modified','desc','startdatum', 'desc') as $evenement) : ?>
        <div class="lg:grid grid-cols-12 bg-red mt-8 text-white" id="<?php echo $evenement->id() ?>">
            <?php if (!$evenement->coverimage()->empty()) : ?>
            <figure class="col-span-12 md:col-span-4">
                <img class="h-full w-full object-cover"
                    src="<?= $evenement->images()->find($evenement->coverimage())->width(310)->url() ?>">
            </figure>
            <?php endif; ?>
            <div class="<?php e($evenement->coverimage()->empty(), 'lg:col-span-12', 'lg:col-span-8'); ?> p-8">
                <?php if (!$evenement->startdatum()->empty()) : ?>
                <p class="text-sm">
                    <?php echo $evenement->startdatum()->toDate('dd-mm-yy'); ?>
                    <?php if (!$evenement->einddatum()->empty()) : ?> tot
                    <?php echo $evenement->einddatum()->html() ?><?php endif; ?>
                </p>
                <?php endif; ?>
                <h3 class="mb-4"><?php echo $evenement->title()->html() ?></h3>
                <div>
                    <?php if ($evenement->text()->empty()) : ?>
                    <?php echo $evenement->highlight() ?>
                    <?php else : ?>
                    <?php echo $evenement->text()->kirbytext() ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</section>
<?php endif; ?>