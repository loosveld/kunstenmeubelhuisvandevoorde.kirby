<footer class="bg-red pt-12 pb-8 text-sm leading-loose text-white ">
    <div class="container">
        <div class="lg:grid grid-cols-12">
            <div class="lg:col-span-4 mb-12" id="contact">
                <h3 class="uppercase mb-8">Contactgegevens</h3>
                <div>
                    <i class="fas fa-map-marker w-4 mr-2 text-center" aria-hidden="true"></i><a
                        href="https://goo.gl/maps/mfEhqMCQFFq" target="_blank"><?php echo $site->address() ?></a><br />
                    <i class="fas fa-envelope w-4 mr-2 text-center" aria-hidden="true"></i><a
                        href="mailto:<?php echo $site->email() ?>"><?php echo $site->email() ?></a><br />
                    <i class="fas fa-phone w-4 mr-2 text-center" aria-hidden="true"></i><?php echo $site->tel() ?><br />
                    <i class="fas fa-info w-4 mr-2 text-center" aria-hidden="true"></i>Vlot bereikbaar met openbaar
                    vervoer<br />
                    <i class="fas fa-info w-4 mr-2 text-center" aria-hidden="true"></i>300m vanaf het station

                    <div class="flex mb-8">
                        <a href="https://www.facebook.com/meubelenVDV/"
                            class="fab fa-facebook-square w-4 mr-2 text-center text-4xl no-underline block mt-8"
                            aria-hidden="true" target="_blank"> </a>
                        <a href="https://www.instagram.com/vandevoorde.art/"
                            class="fab fa-instagram w-4 mr-2 text-center text-4xl no-underline block mt-8 ml-8"
                            aria-hidden="true" target="_blank"> </a>
                    </div>

                    <a href="https://kunstenmeubelhuisvandevoorde.us10.list-manage.com/subscribe/post?u=aa196448bb851cf5f501af055&id=59cdb442c0"
                        target="_blank"
                        class="cursor-pointer text-red flex items-center justify-between with-arrow-right mb-8 btn font-title p-4 bg-white no-underline uppercase font-bold mt-4 xl:w-3/4 mr-8">

                        Schrijf u in<br />op onze nieuwsbrief
                        <svg height="50" width="50" viewBox="0 0 50 50" class="ml-4 inline">
                            <polyline fill="none" stroke="#d00025" stroke-width="2px" points="20,15 30,25 20,35" />
                            <polyline fill="none" stroke="#d00025" stroke-width="2px" points="0,25 30,25" />
                        </svg>
                    </a>

                </div>
            </div>

            <div class="lg:col-span-5 mb-12" id="openingsuren">
                <h3 class="uppercase mb-8">OPENINGSUREN</h3>
                <?php echo $site->hours()->kirbyText(); ?>
            </div>

            <div class="lg:col-span-3 mb-8">
                <h3 class="uppercase  mb-8">ONTDEK OOK</h3>
                <a href="https://www.artgalleryvandevoorde.be/" target="_blank"
                    class="bg-white flex items-center justify-center p-4">
                    <img src="/assets/images/Vandevoorde-Artgallery-wit-zwart.svg" class="">
                </a>
            </div>

            <!-- Begin Mailchimp Signup Form -->
            <!--<div class="lg:col-span-12 mb-16" id="mc_embed_signup">

                <a href="https://kunstenmeubelhuisvandevoorde.us10.list-manage.com/subscribe/post?u=aa196448bb851cf5f501af055&id=59cdb442c0"
                    class="cursor-pointer text-white flex items-center justify-between sweep-to-top with-arrow-right mb-8 btn font-title p-4 bg-red no-underline uppercase font-bold mt-4 ">
                    Schrijf u in op onze nieuwsbrief
                    <svg height="50" width="50" viewBox="0 0 50 50" class="ml-4 inline">
                        <polyline fill="none" stroke="white" stroke-width="2px" points="20,15 30,25 20,35" />
                        <polyline fill="none" stroke="white" stroke-width="2px" points="0,25 30,25" />
                    </svg>
                </a>



                <form
                    action="https://kunstenmeubelhuisvandevoorde.us10.list-manage.com/subscribe/post?u=aa196448bb851cf5f501af055&amp;id=59cdb442c0"
                    method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form"
                    class=" grid grid-cols-12 gap-8" target="_blank">



                    <div class="lg:col-span-6 mb-16">

                        <div id="mc_embed_signup_scroll">

                            <div class="mc-field-group">

                                <input type="email" value="" name="EMAIL" id="mce-EMAIL" placeholder="Uw emailadres"
                                    class="required email bg-white focus:outline-none focus:shadow-outline py-2 px-4 text-black lg:w-full appearance-none leading-normal h-12 mb-4">
                            </div>
                            <div class="mc-field-group">
                                <input type="text" value="" name="FNAME" id="mce-FNAME" placeholder="Uw voornaam"
                                    class="required email bg-white focus:outline-none focus:shadow-outline py-2 px-4 text-black lg:w-full appearance-none leading-normal h-12 mb-4">
                            </div>
                            <div class="mc-field-group">
                                <input type="text" value="" name="LNAME" id="mce-LNAME" placeholder="Uw familienaam"
                                    class="required email bg-white focus:outline-none focus:shadow-outline py-2 px-4 text-black lg:w-full appearance-none leading-normal h-12 mb-4">
                            </div>


                        </div>
                    </div>

                    <div class="lg:col-span-6">
                        <div id="mergeRow-gdpr" class="mergeRow gdpr-mergeRow content__gdprBlock mc-field-group mb-8">
                            <div class="mc-field-group input-group">
                                <div class="mb-1"><strong>Uw interesses? <span class="asterisk">*</span></strong></div>
                                <div class="flex">
                                    <span class="mr-4">
                                        <input type="radio" value="Kunst" name="VOORKEUR" id="mce-VOORKEUR-0"
                                            class="hidden">
                                        <label for="mce-VOORKEUR-0" class="flex items-center cursor-pointer">
                                            <span
                                                class="w-4 h-4 inline-block mr-1 rounded-full border border-grey"></span>
                                            Kunst</label>
                                    </span>

                                    <span class="ml-2 mr-4">
                                        <input type="radio" value="Meubelen" name="VOORKEUR" id="mce-VOORKEUR-1"
                                            class="hidden">
                                        <label for="mce-VOORKEUR-1" class="flex items-center cursor-pointer">
                                            <span
                                                class="w-4 h-4 inline-block mr-1 rounded-full border border-grey"></span>
                                            Meubelen</label>
                                    </span>

                                    <span class="ml-2 mr-4">
                                        <input type="radio" value="Beiden" name="VOORKEUR" id="mce-VOORKEUR-2" checked
                                            class="hidden">
                                        <label for="mce-VOORKEUR-2" class="flex items-center cursor-pointer">
                                            <span
                                                class="w-4 h-4 inline-block mr-1 rounded-full border border-grey"></span>
                                            Beiden</label>
                                    </span>

                                </div>
                            </div>
                            <div class="content__gdpr mt-4">
                                <strong>
                                    Op welke manieren wilt u van ons horen?
                                </strong>
                                <fieldset class="mc_fieldset gdprRequired mc-field-group" name="interestgroup_field">
                                    <label class="checkbox subfield " for="gdpr_4937">
                                        <input type="checkbox" id="gdpr_4937" name="gdpr[4937]" value="Y"
                                            class="av-checkbox mr-2"><span>Email</span>
                                    </label>
                                    <label class="checkbox subfield " for="gdpr_4941">
                                        <input type="checkbox" id="gdpr_4941" name="gdpr[4941]" value="Y"
                                            class="av-checkbox ml-4 mr-2 "><span>Directe post</span>
                                    </label>
                                    <label class="checkbox subfield " for="gdpr_4945">
                                        <input type="checkbox" id="gdpr_4945" name="gdpr[4945]" value="Y"
                                            class="av-checkbox ml-4 mr-2 "><span>Aangepaste online advertenties</span>
                                    </label>
                                </fieldset>
                                <p class="hidden">U kunt op elk moment van gedachten veranderen door op de link
                                    Abonnement opzeggen
                                    te klikken in de voettekst van elke email die u van ons ontvangt, of door
                                    contact met ons op te nemen via info@kunstenmeubelhuisvandevoorde.be. We zullen
                                    uw gegevens met respect behandelen. Bezoek onze website voor meer informatie
                                    over onze privacypraktijken. Door hieronder te klikken, stemt u ermee in dat wij
                                    uw gegevens mogen verwerken in overeenstemming met deze voorwaarden.</p>
                            </div>
                            <div class="content__gdprLegal hidden">
                                <p>We use Mailchimp as our marketing platform. By clicking below to subscribe, you
                                    acknowledge that your information will be transferred to Mailchimp for
                                    processing. <a href="https://mailchimp.com/legal/" target="_blank">Learn more
                                        about Mailchimp's privacy practices here.</a></p>
                            </div>
                        </div>
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>
                        <div style="position: absolute; left: -5000px;" aria-hidden="true">
                            <input type="text" name="b_aa196448bb851cf5f501af055_59cdb442c0" tabindex="-1" value="">
                        </div>
                        <div class="clear"><input type="submit" value="Inschrijven" name="subscribe"
                                id="mc-embedded-subscribe"
                                class="cursor-pointer text-white hover:text-red hover:bg-white bg-black inline-flex items-center border-none justify-between btn font-title py-2 px-4 uppercase font-bold  h-12 ">
                        </div>
                    </div>
                </form>
            </div>-->

            <!--End mc_embed_signup-->
        </div>
    </div>
</footer>