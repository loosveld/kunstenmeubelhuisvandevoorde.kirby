document.onreadystatechange = function () {
    const loader = document.querySelector("#loader");
    const main = document.querySelector("main");
    if (loader) {
        if (document.readyState === "complete") {
            loader.style.visibility = "hidden";
            main.style.visibility = "visible";
        } else {
            main.style.visibility = "hidden";
        }
    }
};
