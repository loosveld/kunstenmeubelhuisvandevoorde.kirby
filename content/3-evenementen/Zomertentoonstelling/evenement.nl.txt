Title: Zomer tentoonstelling

----

Startdatum: 2017-06-01

----

Einddatum: 2017-09-30

----

Coverimage: zomertentoonstelling.png

----

Text: 

>De zomer is licht en vrolijk
>het leven ontspant zich
>het licht sprankelt
>de natuur is speels

De zomer maakt ons vrolijk en dit gevoel hebben onze kunstenaars omhelst. Schilderijen en beelden die bekoren met hun zomerse sprankeling en elektrische spanning van zwoele buien. De koestering van de zomer legden ze elk op hun eigen wijze vast.
Een gevoel dat nazindert en blijft hangen, ook als het herfst en winter wordt en wij juist zo’n behoefte hebben aan de zomerse lichtheid.